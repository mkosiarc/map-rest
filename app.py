from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'crud.sqlite')
db = SQLAlchemy(app)
ma = Marshmallow(app)


class Country(db.Model):
    name = db.Column(db.String(100), primary_key=True)
    visited = db.Column(db.Boolean(), default=False)
    has_images = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<Country %r>' % self.name

class CountrySchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('name', 'visited')

country_schema = CountrySchema()
countries_schema = CountrySchema(many=True)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/visited' , methods=['POST'])
def visited():
    data = request.get_json()
    country_name = data['name']
    visited = data['visited']
    print(visited)
    if visited == "True" or visited=="true" or visited == True:
        changedVisited = True
    else:
        changedVisited = False

    country = Country.query.get(country_name)
    country.visited = changedVisited
    db.session.commit()
    return jsonify({"message": "country " + str(country_name) + " is now set to " + str(visited)})


@app.route('/all_visited', methods=['GET'])
def get_visited_countries():
    countries = Country.query.filter_by(visited=True).all()
    result = countries_schema.dump(countries)
    return jsonify(result)


if __name__ == '__main__':
    app.run(port=8000, debug=True)
